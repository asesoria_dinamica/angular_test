import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { Cliente } from '../models/cliente';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({providedIn: "root"})
export class ClientsService{
    private clientes: Cliente[] = [];
    private clientesActualizados = new Subject<Cliente[]>();

    constructor(private http: HttpClient, private router: Router){

    }

    getClientes(){
        //es para compiar el array a un elemento nuevo, 
        //que no sea una referencia. ya que con this.clientes afectaria al original.
        //return [...this.clients];
        //se hace la peticion al backend por http
        this.http.get<{message: string, clientes:Cliente[]}>('http://localhost:3000/api/clientes')
        .pipe(
            map(clienteData => {
            return clienteData.clientes.map(cliente => {
                return {
                    nombre: cliente.nombre,
                    tipo: cliente.tipo,
                    fecha_alta: cliente.fecha_alta,
                    id: cliente.id
                }
            })
        })).subscribe((clienteTransformado) => {
            this.clientes = clienteTransformado;
            this.clientesActualizados.next([...this.clientes]);
        });
    }

    getClientesActualizadosListener(){
        return this.clientesActualizados.asObservable();
    }

    addCliente(nombre: string, tipo: number, fecha_alta: any){
        const cliente: Cliente = {id:null, nombre: nombre, tipo:tipo , fecha_alta: fecha_alta};
        this.http.post<{message:string, clienteId:number}>("http://localhost:3000/api/clientes", cliente).subscribe((data)=>{
            const id = data.clienteId;
            cliente.id = id;
            console.log(data.clienteId);
            this.clientes.push(cliente);
            this.clientesActualizados.next([...this.clientes]);
            this.router.navigate(["/reporte_clientes"]);
        });
        
    }

    updateCliente(id:number, nombre:string, tipo:number, fecha_alta:string){
        const cliente: Cliente = {id:id, nombre:nombre, tipo:tipo, fecha_alta:fecha_alta};
        this.http.put("http://localhost:3000/api/clientes/"+id, cliente)
        .subscribe(response => {
            console.log(response);
            this.router.navigate(["/reporte_clientes"]);
        });
    }

    getCliente(id: number){
        return this.http.get<{cliente:Cliente}>(
            "http://localhost:3000/api/clientes/" + id
          ); 
    }
}