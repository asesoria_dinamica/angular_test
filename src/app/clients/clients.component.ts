import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, NgForm, FormGroupDirective } from '@angular/forms';
import { Cliente } from "../models/cliente";
import { ClientsService } from './clients.service';
import { formatDate } from '@angular/common';
import { ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})

export class ClientsComponent implements OnInit {
  cliente: Cliente;
  fecha_alta;
  tipo=0;
  isLoading = false;
  private mode = 'alta_cliente';
  clienteId: number;
  formDirective: FormGroupDirective;
  clienteForm: FormGroup;
  tipos = [
    {
      id:1,
      nombre:"Referido"
    },
    {
      id:2,
      nombre:"Amigo"
    },
    {
      id:3,
      nombre:"Otro"
    }]; 
  
  constructor( public clientesServicio: ClientsService, public route: ActivatedRoute) {
    this.clienteForm = this.createFormGroup();
  }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) =>{
      if(paramMap.has("clienteId")){
        this.mode = "editar_cliente";
        this.clienteId = parseInt(paramMap.get("clienteId"));
        this.isLoading = true;
        this.clientesServicio.getCliente(this.clienteId).subscribe( clienteData => {
          this.isLoading = false;
          this.cliente = {
            id:clienteData.cliente[0].id,
            nombre: clienteData.cliente[0].nombre,
            tipo: clienteData.cliente[0].tipo,
            fecha_alta: clienteData.cliente[0].fecha_alta
          };
          this.setEditValues();
        });
      } else {
        this.mode = "alta_cliente";
        this.clienteId = null;
      }
    }); 
  }

  setEditValues(){
    const toSelect = this.tipos
    this.clienteForm.setValue({
      nombre:this.cliente.nombre,
      tipo: this.cliente.tipo,
      fecha_alta: this.cliente.fecha_alta
    });
    this.fecha_alta = this.cliente.fecha_alta;
    this.tipo = this.cliente.tipo;
    
  }
  
  createFormGroup() {
    return new FormGroup({
        nombre: new FormControl("",[
          Validators.required]),
        tipo: new FormControl(),
        fecha_alta: new FormControl("",[
          Validators.required])
    });
  }


  onSubmit(formDirective: FormGroupDirective){

    this.clienteForm.value.fecha_alta = formatDate(this.clienteForm.value.fecha_alta,"yyyy-MM-dd","en-US");
    // Make sure to create a deep copy of the form-model
    const result: Cliente = Object.assign({}, this.clienteForm.value);
    this.isLoading = true;
    if(this.mode == "alta_cliente"){
      this.clientesServicio.addCliente(result.nombre, result.tipo, result.fecha_alta);
    } else {
      this.clientesServicio.updateCliente(this.clienteId,result.nombre, result.tipo, result.fecha_alta);
    }
    formDirective.resetForm();
  }

}
