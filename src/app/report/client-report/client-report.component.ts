import { Component, OnInit, OnDestroy } from '@angular/core';
import { Cliente } from 'src/app/models/cliente';
import { ClientsService } from 'src/app/clients/clients.service';
import { Subscription, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: 'app-client-report',
  templateUrl: './client-report.component.html',
  styleUrls: ['./client-report.component.css']
})

export class ClientReportComponent implements OnInit, OnDestroy {
  isLoading = false;
  clientes:Cliente[] = [];
  private clientesSubs: Subscription;
  private onDestroy$ = new Subject();

  constructor(public clientesServicio: ClientsService) { }

  ngOnInit() {
    this.isLoading = true;
    this.clientesServicio.getClientes();
    this.clientesServicio.getClientesActualizadosListener().pipe(takeUntil(this.onDestroy$))
    .subscribe((clients: Cliente[])=>{
      this.isLoading = false;
      this.clientes = clients;
    });
  }
  ngOnDestroy(){
    //this.clientesSubs.unsubscribe();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
