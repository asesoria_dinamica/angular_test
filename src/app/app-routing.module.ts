import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ClientReportComponent } from './report/client-report/client-report.component';
import { ClientsComponent } from './clients/clients.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';

const routes: Routes = [
    {path: "", component: AppComponent},
    {path: "alta_cliente", component: ClientsComponent},
    {path: "editar_cliente/:clienteId", component: ClientsComponent},
    {path: "reporte_clientes", component: ClientReportComponent},
    {path: "login", component: LoginComponent},
    {path: "signup", component: SignupComponent},

    //{path: "reporte_clientes/:clienteId", component: ClientReportComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}