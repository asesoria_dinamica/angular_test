const express = require("express");
const db = require("../db");
const bcrypt = require("bcrypt");

const router = express.Router();

router.post("/signup",(req,res,next) => {
    bcrypt.hash(req.body.password, 10)
    .then(hash => {
        const user = {
            email: req.body.email,
            password: hash
        };
        db.query("INSERT INTO users (email, password) VALUES (?,?)",[user.email, user.password],function(error,result){
            if(error){
                console.log('Error in query');  
            } else {
                console.log("Usuario Agregado");
                console.log(result.insertId);
                //codigo 201 significa que todo esta OK y se creo un nuevo recurso
                res.status(201).json({
                    message:"Cliente agregado correctamente",
                    result: result
                });
            }
        });
    });
});

module.exports = router;