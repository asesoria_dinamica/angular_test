const express = require("express");
const Cliente = require("../models/cliente");
const moment = require('moment');
const db = require("../db");

const router = express.Router();

var now = moment().format("YYYY-MM-DD HH:mm:ss");

router.get('', function(req,resp){
    db.getConnection(function(error, tempCont){
        var values = [];
        if(!!error){
            tempCont.release();
            console.log('Error');
        } else {
            console.log("Conected at pool");
            tempCont.query("SELECT * FROM clientes",function(error,rows,fields){
                if(error){
                    console.log('Error in query');  
                } else {
                    console.log("Response: "+ rows[0] + now);
                    resp.status(200);
                    resp.json({
                        message: 'Jalo este show',
                        clientes: rows
                    });
                }
            });
        }
    });
});

router.get('/:id', function(req,resp){
    db.getConnection(function(error, tempCont){
        var values = [];
        if(!!error){
            tempCont.release();
            console.log('Error');
        } else {
            console.log("Conected at pool");
            tempCont.query("SELECT * FROM clientes WHERE id = ?",[req.params.id],function(error,row,fields){
                if(error){
                    console.log('Error in query');  
                } else {
                    console.log("Response: "+ row[0]);
                    resp.status(200);
                    resp.json({
                        cliente: row
                    });
                }
            });
        }
    });
});


var timestamp = moment().format("YYYY-MM-DD HH:mm:ss");

router.post("", (req, res,next) => {
    const cliente = req.body;
    console.log(cliente);
    db.getConnection(function(error, tempCont){
        var values = [];
        if(!!error){
            tempCont.release();
            console.log('Error');
        } else {
            console.log("Conected at pool");
            tempCont.query("INSERT INTO clientes (nombre, tipo, fecha_alta, ts_alta, ts_modificacion,modificado_por) VALUES (?,?,?,?,?,?)",[cliente.nombre, cliente.tipo, cliente.fecha_alta, timestamp, timestamp,1],function(error,result){
                if(error){
                    console.log('Error in query');  
                } else {
                    console.log("Cliente Agregado");
                    console.log(result.insertId);
                    //codigo 201 significa que todo esta OK y se creo un nuevo recurso
                    res.status(201).json({
                        message:"Cliente agregado correctamente",
                        clienteId: result.insertId
                    });
                }
            });
        }
    });
});

router.put("/:id",(req, res, next) => {
    const id = req.params.id;
    const cliente = req.body;
    db.getConnection(function(error, tempCont){
        if(!!error){
            tempCont.release();
            console.log('Error');
        } else {
            console.log("Conected at pool");
            tempCont.query("UPDATE clientes SET nombre = ?, tipo = ?, fecha_alta = ?, ts_modificacion = ?, modificado_por = 2 WHERE id = ?",
            [cliente.nombre, cliente.tipo, cliente.fecha_alta, timestamp, id],function(error,result){
                if(error){
                    console.log('Error in query');  
                } else {
                    console.log("Cliente Actualizado");
                    console.log(result.insertId);
                    //codigo 201 significa que todo esta OK y se creo un nuevo recurso
                    res.status(200).json({
                        message:"Cliente actualizado correctamente"
                    });
                }
            });
        }
    });
});

// app.post('/cliente', function(req,resp){
//     db.getConnection(function(error, tempCont){
//         var values = [];
//         if(!!error){
//             tempCont.release();
//             console.log('Error');
//         } else {
//             console.log("Conected at pool");
//             tempCont.query("SELECT * FROM clientes",function(error,rows,fields){
//                 if(error){
//                     console.log('Error in query');  
//                 } else {
//                     console.log("Response: "+ rows[0] + now);
//                     resp.status(200);
//                     resp.json({
//                         message: 'Jalo este show',
//                         clientes: rows
//                     });
//                 }
//             });
//         }
//     });
// });

module.exports = router;