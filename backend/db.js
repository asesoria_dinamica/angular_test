const mysql = require("mysql");

const connection = mysql.createPool({
    //config properties
    connectionLimit: 100,
    host:'localhost',
    user: 'root',
    password: '',
    database: 'angulo'
});

connection.getConnection(function(error, tempCont){
    if(!!error){
        tempCont.release();
        console.log('Error');}
});

module.exports = connection;