//npm install --save express
const express = require('express');
const db = require("./db");
const moment = require('moment');
// npm install --save body-parser se ocupa para esta funcionalidad
const bodyParser = require('body-parser');
const clientesRoutes = require("./routes/clientes");
const userRoutes = require("./routes/user");

const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:false}));

app.use((req,res,next) => {
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.setHeader('Access-Control-Allow-Headers', "Origin, X-Request-With, Content-Type, Accept");
    res.setHeader('Access-Control-Allow-Methods', "GET, POST, PUT, PATCH, DELETE, OPTIONS");
    next();
});

app.use("/api/clientes", clientesRoutes);
app.use("/api/user", userRoutes);

module.exports = app;